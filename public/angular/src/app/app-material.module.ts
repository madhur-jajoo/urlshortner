import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatListModule,
  MatIconModule,
  MatTabsModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule
} from '@angular/material';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    MatListModule,
    MatIconModule,

    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
  exports: [
    BrowserAnimationsModule,
    MatListModule,
    MatIconModule,

    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
})
export class AppMaterialModule {}
