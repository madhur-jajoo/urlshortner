import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public login_form: FormGroup;
  public processing: Boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
    this.login_form = this.formBuilder.group({
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.minLength(5),
          Validators.maxLength(255),
        ],
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(35),
        ],
      ],
    });
  }

  disableForm() {
    this.login_form.controls['email'].disable();
    this.login_form.controls['password'].disable();
  }

  enableForm() {
    this.login_form.controls['email'].enable();
    this.login_form.controls['password'].enable();
  }

  onLogin() {
    this.processing = true;
    this.disableForm();

    const user = {
      email: this.login_form.get('email').value,
      password: this.login_form.get('password').value,
    };

    this.authService.loginUser(user).subscribe(
      result => {
        console.log(result);
        this.router.navigateByUrl('/user');
      },
      error => {
        console.log(error);
        window.alert(error);
        this.login_form.reset();
        this.processing = false;
        this.enableForm();
      }
    );
  }
}
