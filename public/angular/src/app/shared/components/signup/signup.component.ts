import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  public signup_form: FormGroup;
  public processing: Boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
    this.signup_form = this.formBuilder.group(
      {
        first_name: [],
        last_name: [],
        email: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(255),
            this.validateEmail,
          ]),
        ],
        password: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(35),
          ]),
        ],
        confirm: ['', Validators.required],
      },
      {
        validator: this.matchingPasswords('password', 'confirm'),
      }
    );
  }

  validateEmail(controls) {
    const regExp = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { validateEmail: true };
    }
  }

  matchingPasswords(password, confirm) {
    return (group: FormGroup) => {
      if (group.controls[password].value === group.controls[confirm].value) {
        return null;
      } else {
        return { matchingPasswords: true };
      }
    };
  }

  disableForm() {
    this.signup_form.controls['first_name'].disable();
    this.signup_form.controls['last_name'].disable();
    this.signup_form.controls['email'].disable();
    this.signup_form.controls['password'].disable();
    this.signup_form.controls['confirm'].disable();
  }

  enableForm() {
    this.signup_form.controls['first_name'].enable();
    this.signup_form.controls['last_name'].enable();
    this.signup_form.controls['email'].enable();
    this.signup_form.controls['password'].enable();
    this.signup_form.controls['confirm'].enable();
  }

  register() {
    this.disableForm();
    const user = {
      first_name: this.signup_form.get('first_name').value,
      last_name: this.signup_form.get('last_name').value,
      email: this.signup_form.get('email').value,
      password: this.signup_form.get('password').value,
    };

    this.authService.registerUser(user).subscribe(
      result => {
        console.log(result);
        this.router.navigateByUrl(window.location.host);
      },
      error => {
        console.log(error);
        window.alert(error);
        this.signup_form.reset();
        this.processing = false;
        this.enableForm();
      }
    );
  }
}
