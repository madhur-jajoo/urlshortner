import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class CookieService {

    constructor(
        @Inject(DOCUMENT) private document: any
    ) { }

    check(name: string): boolean {
        name = encodeURIComponent(name);
        const regExp: RegExp = this.getCookieRegExp(name);
        const exists: boolean = regExp.test(this.document.cookie);
        return exists;
    }

    // checkAndValidate(name: string): boolean {
    //     if (this.check(name)) {
    //         console.log(this.document.cookie);
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    get(name: string): string {
        if (this.check(name)) {
            name = encodeURIComponent(name);
            const regExp: RegExp = this.getCookieRegExp(name);
            const result: RegExpExecArray = regExp.exec(this.document.cookie);
            return decodeURIComponent(result[1]);
        } else {
            return '';
        }
    }

    getAll(): {} {
        const cookies: {} = {};
        const document: any = this.document;
        if (document.cookie && document.cookie !== '') {
            const split: Array<string> = document.cookie.split(';');
            for (let i = 0; i < split.length; i += 1) {
                const currentCookie: Array<string> = split[i].split('=');
                currentCookie[0] = currentCookie[0].replace(/^ /, '');
                cookies[decodeURIComponent(currentCookie[0])] = decodeURIComponent(currentCookie[1]);
            }
        }
        return cookies;
    }

    set(
        name: string,
        value: string,
        expires?: number | Date,
        path?: string,
        domain?: string,
        secure?: boolean
    ): void {
        let cookieString: string = encodeURIComponent(name) + '=' + encodeURIComponent(value) + ';';
        if (expires) {
            if (typeof expires === 'number') {
                const dateExpires: Date = new Date(new Date().getTime() + expires);
                cookieString += 'expires=' + dateExpires.toUTCString() + ';';
            } else {
                cookieString += 'expires=' + expires.toUTCString() + ';';
            }
        }

        if (path) {
            cookieString += 'path=' + path + ';';
        }

        if (domain) {
            cookieString += 'domain=' + domain + ';';
        }

        if (secure) {
            cookieString += 'secure;';
        }

        this.document.cookie = cookieString;
    }


    delete(name: string, path?: string, domain?: string): void {
        this.set(name, '', -1, path, domain);
    }

    deleteAll(path?: string, domain?: string): void {
        const cookies: any = this.getAll();
        for (const cookieName in cookies) {
            if (cookies.hasOwnProperty(cookieName)) {
                this.delete(cookieName, path, domain);
            }
        }
    }

    private getCookieRegExp(name: string): RegExp {
        const escapedName: string = name.replace(/([\[\]\{\}\(\)\|\=\;\+\?\,\.\*\^\$])/ig, '\\$1');
        return new RegExp('(?:^' + escapedName + '|;\\s*' + escapedName + ')=(.*?)(?:;|$)', 'g');
    }
}