import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpResponse,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, map, catchError } from 'rxjs/operators';

import { environment } from './../../../environments/environment';
import { CookieService } from './cookie.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private API_URL = environment.API_URL;

  private options;

  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService
  ) {}

  private errorHandler(error: HttpErrorResponse): Observable<{}> {
    return throwError(
      error.error.error.message ||
        error.error.message ||
        error.message ||
        'Server Error'
    );
  }

  private createAuthenticationHeaders(): void {
    console.log('aaaaa', this.cookieService.get('x-access-token'));
    this.options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'x-access-token': this.cookieService.get('x-access-token'),
      }),
    };
  }

  registerUser(user: any): Observable<any> {
    return this.httpClient.post(this.API_URL + '/user', user).pipe(
      retry(3),
      map((res: HttpResponse<JSON>) => {
        return res;
      }),
      catchError(this.errorHandler)
    );
  }

  loginUser(user: any): Observable<any> {
    return this.httpClient.post(this.API_URL + '/user/login', user).pipe(
      retry(3),
      map((res: HttpResponse<JSON>) => res),
      catchError(this.errorHandler)
    );
  }

  isLoggedIn(): Boolean {
    return this.cookieService.check('x-access-token');
  }

  logOutUser(): void {
    this.cookieService.deleteAll();
    return;
  }

  getLoggedInUser(): {} {
    this.createAuthenticationHeaders();
    return this.httpClient.get(this.API_URL + '/user', this.options).pipe(
      retry(3),
      map(res => {
        return res;
      }),
      catchError(this.errorHandler)
    );
  }
}
