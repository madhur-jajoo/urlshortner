"use strict";

module.exports = {
  NODE_ENV: process.env.NODE_ENV,
  PORT: process.env.PORT,
  HOSTNAME: process.env.HOSTNAME,
  DB_HOST: process.env.DB_HOST,
  DB_PORT: process.env.DB_PORT,
  DB_NAME: process.env.DB_NAME,
  RD_PORT: process.env.RD_PORT,
  RD_HOST: process.env.RD_HOST,
  JWT_SECRET: process.env.JWT_SECRET,
  MAILER_USERNAME: process.env.MAILER_USERNAME,
  MAILER_PASSWORD: process.env.MAILER_PASSWORD,
};
