"use strict";

const debug = require("debug")("config:redis");
const config = require("./config");
const redis = require("redis");
const bluebird = require("bluebird");

bluebird.promisifyAll(redis.RedisClient.prototype);

const client = redis.createClient({
  host: config.RD_HOST,
  port: config.RD_PORT,
});

client.on("error", function (err) {
  debug(`Redis Error ${err}`);
  // eslint-disable-next-line no-console
  console.log(`Redis Error ${err}`);
});
client.on("connect", function () {
  debug(`Redis Connected`);
  // eslint-disable-next-line no-console
  console.log(`Redis Connected`);
});
client.on("reconnecting", function () {
  debug(`Redis Reconnecting`);
  // eslint-disable-next-line no-console
  console.log(`Redis Reconnecting`);
});
client.on("end", function () {
  debug(`Redis Server Closed`);
  // eslint-disable-next-line no-console
  console.log(`Redis Server Closed`);
});

module.exports = client;
