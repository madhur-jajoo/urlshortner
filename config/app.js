"use strict";

const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const helmet = require("helmet");
const compression = require("compression");
const cors = require("cors");

const mongoose = require("./mongoose");
const routes = require("./../routes");
const logger = require("./../middlewares/logger");

const app = express();
mongoose.createConnection();

// app.use(function(req, res, next) {
//   res.header('Access-Control-Allow-Origin', '*');
//   res.header('Access-Control-Allow-Headers', '*');
//   next();
// });
app.use(
  cors({
    origin: ["http://localhost:4200", "http://client.hoston.local"],
    credentials: true,
  })
);
app.use(logger());
app.enable("trust proxy");
app.use(
  helmet({
    hidePoweredBy: {
      setTo: "ZendServer 8.5.0,ASP.NET",
    },
  })
);
app.use(compression({}));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "./../public/pages")));

app.use(routes);

module.exports = app;
