"use strict";

module.exports = (response, error) => {
  response.status(400).json({
    success: false,
    error: {
      message: error.message || "Bad Request",
    },
  });
};
