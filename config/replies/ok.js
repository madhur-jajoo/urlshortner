"use strict";

module.exports = (response, data) => {
  response.status(200).json({
    success: true,
    data: data,
  });
};
