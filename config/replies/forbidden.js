"use strict";

module.exports = (response, error) => {
  response.status(403).json({
    success: false,
    error: {
      message: error.message || "Forbidden",
    },
  });
};
