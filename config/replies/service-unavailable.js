"use strict";

module.exports = (response, data) => {
  response.status(503).json({
    success: false,
    data: data,
  });
};
