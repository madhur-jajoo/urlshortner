"use strict";

module.exports = (response, error) => {
  response.status(401).json({
    success: false,
    error: {
      message: error.message || "You are Unauthorized",
    },
  });
};
