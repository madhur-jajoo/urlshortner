"use strict";

module.exports = response => {
  response.status(204).json({
    success: true,
  });
};
