"use strict";

module.exports = (response, error) => {
  response.status(404).json({
    success: false,
    error: {
      message: error.message || "Not Found",
    },
  });
};
