"use strict";

module.exports = {
  accepted: require("./accepted"),
  bad_request: require("./bad-request"),
  created: require("./created"),
  forbidden: require("./forbidden"),
  internal_server_error: require("./internal-server-error"),
  no_content: require("./no-content"),
  not_found: require("./not-found"),
  ok: require("./ok"),
  service_unavailable: require("./service-unavailable"),
  unauthorised: require("./unauthorised"),
};
