"use strict";

module.exports = (response, data) => {
  response.status(202).json({
    success: true,
    data: data,
  });
};
