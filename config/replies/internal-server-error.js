"use strict";

module.exports = (response, error) => {
  response.status(500).json({
    success: false,
    error: {
      message: error.message || "Internal Error",
    },
  });
};
