"use strict";

module.exports = (response, data) => {
  response.status(201).json({
    success: true,
    data: data,
  });
};
