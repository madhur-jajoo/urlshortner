"use strict";

const winston = require("winston");
const path = require("path");
const fs = require("fs");

fs.existsSync(path.join(__dirname, "./..", "/logs")) ||
  fs.mkdirSync(path.join(__dirname, "./..", "/logs"));

const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "info",
      format: winston.format.json(),
      filename: path.join(__dirname, "./..", "/logs", "/combined.log"),
    }),
  ],
  exitOnError: false,
});

module.exports = function() {
  return function(req, res, next) {
    logger.log({
      level: "info",
      message: {
        ip: req.ip || req._remoteAddress || undefined,
        url: req.originalUrl || req.url,
        res: res.statusCode,
        method: req.method,
        httpVersion: req.httpVersion,
        date: new Date().toISOString(),
      },
    });
    next();
  };
};
