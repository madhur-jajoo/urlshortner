## v0.1.0
- bug fixes
- added user api routes
- added nodemailer for sending verification emails
- added replies configs
- jwt for tokenization implemented

## v0.0.1
- added redis for caching
- moved routing to be controlled by nginx

## v0.0.0
- plain HTML built page
- basic url shortner
- express framework use
- use mongoDB for storing and fetching data