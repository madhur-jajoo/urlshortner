# URL Shortner

## Make URLs shorter, simpler and smarter. Get the inside statstics, clicks, locations and much more.

> Lightweight URL shortner built with :
>
> - [Nginx](http://nginx.org) - serving static files and reverse proxying
> - [Node.js](http://nodejs.org) - building the api
> - [MongoDB](https://www.mongodb.com) - database store
> - [Redis](https://redis.io) - caching the recently requested urls
> - [Angular](https://angular.io) - building the client panel

## Before start

- Make sure that Nginx is installed and running
- Run the mongoDB server

```bash
$ mongod
```

- Run the Redis server

```bash
$ redis-server
```

- Make a .env file and set the environment variables as in .env.example

## Quick Start

```bash
git clone https://github.com/madhur-jajoo/urlshortner.git
cd urlshortner
npm install
cp .env.example .env
chmod +x deploy.sh
sudo ./deploy.sh
npm start
```

## Debug

> Package used : [debug](https://www.npmjs.com/package/debug)

```bash
$ npm run debug
```

## Test

```bash
$ npm run test
```

## License

Released under [the MIT license](LICENSE)
