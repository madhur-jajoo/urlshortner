"use strict";

const debug = require("debug")("helpers:mailer");
const nodemailer = require("nodemailer");

const config = require("./../config/config");
const Token = require("./token");

let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: config.MAILER_USERNAME,
    pass: config.MAILER_PASSWORD,
  },
});

const sendVerificationMail = async (user, host) => {
  debug("Sending verification email");
  let token = await Token.createEmailVerificationToken(user);
  let message = `
            <h1>Welcome to HostOn URL Shortner</h1>
            <p>Please click
                <a href="${host}/api/v1/user/verify/${token}">here</a>
                (${host}/api/v1/user/verify/${token})
                to confirm your registration
            </p>
        `;
  let mailOptions = {
    from: config.MAILER_USERNAME,
    to: user.email.address,
    subject: "Welcome | HostOn.xyz",
    html: message,
  };

  await transporter.sendMail(mailOptions, (err, info) => {
    if (err) throw err;
    debug(`Mail sent : ${info.response}`);
  });
};

module.exports.sendVerificationMail = sendVerificationMail;
