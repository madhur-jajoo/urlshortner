"use strict";

const debug = require("debug")("helpers:token");
const jwt = require("jsonwebtoken");

const config = require("./../config/config");
const reply = require("./../config/replies");

const createToken = async user => {
  try {
    debug("Creating user token");
    let token = await jwt.sign(
      {
        _id: user._id,
        email: user.email,
      },
      config.JWT_SECRET,
      {
        expiresIn: 14400,
      }
    );
    debug("Token created");
    return token;
  } catch (err) {
    throw new Error(err.error || err.message || "Token Creation Error");
  }
};

const verifyToken = async (req, res, next) => {
  try {
    let token = await req.headers["x-access-token"];
    debug("Verifying token");
    if (token) {
      req.decoded = await jwt.verify(token, config.JWT_SECRET);
      debug("Token successfully decoded");
      next();
    } else {
      throw new Error("Token not provided");
    }
  } catch (err) {
    debug(err.error || err.message || "Token verify error");
    reply.unauthorised(res, err);
  }
};

const createEmailVerificationToken = async user => {
  try {
    debug("Creating email verification token");
    let token = await jwt.sign(
      {
        _id: user._id,
        token: user.email.verification_token,
      },
      config.JWT_SECRET,
      {
        expiresIn: "7d",
        algorithm: "HS256",
      }
    );
    debug("Email verification token created");
    return token;
  } catch (err) {
    debug(
      err.error || err.message || "Email verification token creation error"
    );
    throw new Error("Error creating email token");
  }
};

const decodeEmailVerificationToken = async token => {
  try {
    debug("Decoding email verificaiton token");
    let decoded = await jwt.verify(token, config.JWT_SECRET);
    debug("Email token successfully verified");
    return {
      _id: decoded._id,
      token: decoded.token,
    };
  } catch (err) {
    debug(err.error || err.message || "Error decoding email token");
    throw new Error("Token not available or expired");
  }
};

module.exports.createToken = createToken;
module.exports.verifyToken = verifyToken;
module.exports.createEmailVerificationToken = createEmailVerificationToken;
module.exports.decodeEmailVerificationToken = decodeEmailVerificationToken;
