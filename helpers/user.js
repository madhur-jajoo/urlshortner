"use strict";

const debug = require("debug")("helpers:user");
const Crypto = require("crypto");
const User = require("mongoose").model("User");

const createUser = async options => {
  debug("Creating user");
  let user = await new User();
  user.email.address = options.email.toLowerCase();
  user.email.verification_token = Crypto.randomBytes(32).toString("hex");
  user.password = options.password;
  await user.save();
  debug("User created");
  return user;
};

const loginUser = async options => {
  debug("Logging in user");
  let user = await User.findOne({ "email.address": options.email });
  if (!user || !user.is_active) throw new Error("Email not found");
  if (!user.email.verified) throw new Error("Please verify");
  let valid = await user.comparePassword(options.password);
  if (!valid) throw new Error("Incorrect UserId/Password combination");
  return user;
};

const getUser = async options => {
  debug("Getting user");
  let user = await User.findById(options._id);
  if (!user) throw new Error("User not found");
  return user;
};

const deleteUser = async options => {
  debug("Deleting user");
  let user = await User.findByIdAndRemove(options._id);
  if (!user) throw new Error("Error deleting user");
  return user;
};

module.exports.createUser = createUser;
module.exports.loginUser = loginUser;
module.exports.getUser = getUser;
module.exports.deleteUser = deleteUser;
