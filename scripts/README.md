# Configuring Nginx

- Copy the .conf file to /etc/nginx/{host}/{host}.conf
- Change the required in the conf file
- Test nginx conf 
```bash
$ nginx -t
```
- Reload nginx if test passes
```bash
$ nginx -s reload
```
- Check sginx status 
```bash
$ service nginx status
or
$ systemctl status nginx
```