"use strict";

const debug = require("debug")("models:user");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const SALT_ROUNDS = 9;

const userSchema = new mongoose.Schema(
  {
    first_name: {
      type: String,
      uppercase: true,
    },
    last_name: {
      type: String,
      uppercase: true,
    },
    gender: {
      type: String,
      uppercase: true,
      enum: ["MALE", "FEMALE", "OTHER"],
    },
    username: {
      type: String,
      lowercase: true,
      unique: [true, "Username already present"],
    },
    email: {
      address: {
        type: String,
        required: [true, "Email is required"],
        unique: [true, "Email already present"],
        lowercase: true,
      },
      verification_token: {
        type: String,
      },
      verified: {
        type: Boolean,
        default: false,
      },
    },
    phone: {
      number: {
        type: Number,
        unique: [true, "Phone number already present"],
        minlength: [10, "Incorrect phone number"],
        maxlength: [10, "Incorrect phone number"],
      },
      verification_token: {
        type: String,
      },
      verified: {
        type: Boolean,
        default: false,
      },
    },
    password: {
      type: String,
      required: [true, "Password is required"],
    },
    password_reset_token: {
      type: String,
    },
    is_active: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc, ret) {
        delete ret.__v;
        delete ret.password;
        return ret;
      },
    },
  }
);

userSchema.pre("save", function (next) {
  if (this.isModified("password") || this.isNew) {
    debug("Hashing password before saving");
    this.password = bcrypt.hashSync(this.password, SALT_ROUNDS);
  }
  next();
});

userSchema.methods.comparePassword = function (candidatePassword) {
  debug("Comparing candidate password with the saved password");
  return bcrypt.compareSync(candidatePassword, this.password);
};

module.exports = mongoose.model("User", userSchema);
