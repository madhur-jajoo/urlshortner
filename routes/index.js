"use strict";

const debug = require("debug")("routes:index");
const router = require("express").Router({ strict: false });

const path = require("path");
const apiRoutes = require("./api");
const urlController = require("./../controllers/url");

router.use("/api", apiRoutes);

router.get("/", (req, res) => {
  debug("Sending homepage files");
  res.sendFile(path.join(__dirname, "./public/pages"));
});
router.post("/", urlController.fullUrl);
router.get("/:url_code", urlController.shortUrl);

module.exports = router;
