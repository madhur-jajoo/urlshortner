"use strict";
const debug = require("debug")("routes:api:v1:index");
const router = require("express").Router();


router.use(function(req, res, next) {
  debug("Configuring response object");
  res._json = res.json;

  res.json = function(obj) {
    obj.APIVersion = "v1";
    obj.IP = req.ip;
    res._json(obj);
  };
  next();
});

router.use("/user", require("./user"));

module.exports = router;
