"use strict";

const router = require("express").Router();

const userController = require("./../../../controllers/api/v1/user");
const Token = require("./../../../helpers/token");

router
  .route("/")
  .get(Token.verifyToken, userController.getLoggedInUser)
  .post(userController.createUser)
  .delete(Token.verifyToken, userController.deleteUserById);

router.post("/login", userController.loginUser);
router.get("/verify/:token", userController.verifyUser);

module.exports = router;
