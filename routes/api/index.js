"use strict";

const debug = require("debug")("routes:api:index");
const router = require("express").Router();

router.use("/v1", require("./v1"));

router.all("/", (req, res) => {
  debug(`/api got hit by IP ${req.ip}`);
  res.send("<h1>Nothing here yet</h1>");
});

module.exports = router;
