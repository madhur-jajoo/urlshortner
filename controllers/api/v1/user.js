"use strict";

const debug = require("debug")("controllers:api:v1:user");

const Token = require("./../../../helpers/token");
const userHelper = require("./../../../helpers/user");
const Mailer = require("./../../../helpers/mailer");
const reply = require("./../../../config/replies");

const createUser = async (req, res) => {
  try {
    debug("Creating user");
    if (!(req.body.email && req.body.password))
      throw new Error("Incomplete Details");
    let user = await userHelper.createUser(req.body);
    if (!user) throw new Error("Error creating user");
    Mailer.sendVerificationMail(user, req.headers.host);
    debug("User created successfully");
    reply.created(res, `${user.email.address} created`);
  } catch (err) {
    if (err.code == 11000) {
      err["message"] = `Email ${req.body.email} already registered`;
      debug(err);
      reply.forbidden(res, err);
    } else {
      err["message"] = await err.message.substring(
        err.message.lastIndexOf(":") + 1
      );
      debug(err);
      reply.bad_request(res, err);
    }
  }
};

const verifyUser = async (req, res) => {
  try {
    debug("Verifying user email");
    let data = await Token.decodeEmailVerificationToken(req.params.token);
    let user = await userHelper.getUser(data);
    if (user.email.verified) throw new Error("User already verified");
    if (user.email.verification_token === data.token) {
      user.email.verified = true;
      user.email.verification_token = undefined;
      user.is_active = true;
      await user.save();
      debug("User verified successfully");
      reply.ok(res, `${user.email.address} verified`);
    } else throw new Error("Invalid token");
  } catch (err) {
    err["message"] = err.toString().split(/[:\n]/)[1];
    debug(err);
    reply.bad_request(res, err);
  }
};

const loginUser = async (req, res) => {
  try {
    debug("Logging in user");
    if (!(req.body.email && req.body.password))
      reply.bad_request(res, { message: "Provide email/password" });

    let user = await userHelper.loginUser(req.body);
    let token = await Token.createToken(user);
    debug("User logged in successfully");
    res.cookie("x-access-token", token);
    debug("Set response token cookie");
    reply.ok(res, token);
  } catch (err) {
    err["message"] = err.message.substring(err.message.lastIndexOf(":") + 1);
    debug(err);
    reply.bad_request(res, err);
  }
};

const getLoggedInUser = async (req, res) => {
  try {
    debug("Getting logged in user");
    let user = await userHelper.getUser(req.decoded);
    reply.ok(res, user);
  } catch (err) {
    err["message"] = err.toString().split(/[:\n]/)[1];
    debug(err);
    reply.forbidden(res, err);
  }
};

const deleteUserById = async (req, res) => {
  try {
    debug("Deleting User");
    let user = await userHelper.deleteUser(req.decoded);
    reply.ok(res, `${user.email.address} deleted!`);
  } catch (err) {
    err["message"] = err.toString().split(/[:\n]/)[1];
    debug(err);
    reply.internal_server_error(res, err);
  }
};

module.exports.createUser = createUser;
module.exports.verifyUser = verifyUser;
module.exports.loginUser = loginUser;
module.exports.getLoggedInUser = getLoggedInUser;
module.exports.deleteUserById = deleteUserById;
